#! /bin/bash

#Llistar totes les lines /etc/group i per cada lini ocupacio de home.
function allgroup(){
	while read -r line
	do
		gid=$(echo $line | cut -d: -f3)
		if [ $gid -ge 0 -a $gid - le 100 ];then
			gidsize $gid
		fi
	done < /etc/group
}

#Llistar tots els Gi en ordre creixent,l'ocupacio dehome de user.
function allgidsize(){
	llistarGids=$(cut -d: -f4 /etc/passwd | sort -g | uniq)
	for gid in $llistaGids
	do
		gidsize $gid
	done
}
#Gids ,mostra al usuari que pertany a aquest grup ,ocupacio de dis del seu home
function gidsize(){
	llistarLogins=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd |cut -d: -f1 |sort 
	for login in $llistaLogins
	do
		fsize $login
	done
}

#Gid com arg, llistar els logins dels usuari de aquest group
# com a principal i gid valid.
function grepgid(){
	if [ $# -ne 1 ]
	then 
	return 1
	fi
	gid=$1
	grep "^[^:]*:[^:]*:$gid:" /etc/group &</dev/null
	if [ $? -ne 0 ];then
		return 2
	fi
	grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1
}
