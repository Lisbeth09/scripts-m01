#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:
#Exemples while
#-------------------
# 8) Procesar linia a linia un fitxer.
fileIn=$1
num=1

while read -r line
do
 chars=$(echo $line | wc -c)
 echo "$num: ($chars)  $line" | tr 'a-z' 'A-Z'
      ((num++))
done < $fileIn
exit 0



#7) Numerar i mostrar en majúscules.
# stadin
num=1 
while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
    ((num++))
done 
exit 0 



6) Itera linia a linia fins a token
# (per exemple FI)
TOKEN="FI"
num=1
read -r line
while [ "$line" != $TOKEN ]
do 
  echo "$num: $line"
  read -r line
  ((num++))
done
exit 0



#5) Numerar les linies rebudes 
num=1
while read -r line 
do 
  echo "$num: $line"
  ((num++))
done

exit 0
#4) Processar stadin linia a linia
while read -r line 
do
  echo $line
done
exit 0



#3)Iterar la llista d'arguments 
#   (ojo! usar normalmente for)
while [ -n "$1" ]
do 
  echo "$1 $# $*"
  shift
done
exit 0



#2)Contador decrementa valor rebut

# narg ...0 
MIN=0
num=$1
while [ $num -ge $MIN ]
do
  echo -n "$num"
   ((num--))
done
exit 0





#1) Mostrar numero del 1 al 10
MAX=10
num=1
while  [  $num -le $MAX ]
do
  echo -n "$num" 
   ((numm++))
done

exit 0

