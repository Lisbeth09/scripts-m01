#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:Fes un programa que rep com a argument números de més (un o més) i indicia per a cada mes rebut quants dies té el més.
#-------------------


#3)Fer un comptador des de zero fins al valor indicat per l'argument rebut.

if [ $# -eq 0 ]; then
   echo "Error: número d'argument incorrecte."
   echo "Usage: $0 num"
   exit 1
fi

num=0
MAX=$1
while  [ $num -le $MAX ]
do    
      echo "$num"
      ((num++))  
done 
exit 0





#2) Mostrar els arguments rebuts línia a línia,tot numerànt-los
num=1
for linia in $*
do  
    echo "$num: $linia"
       ((num++)) 
done 
exit 0



#1)
num=1
while read -r line
do 
     echo "$num: $num"
        ((num++))
done 
exit 0
  
