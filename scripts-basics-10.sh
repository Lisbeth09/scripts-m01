#! /bin/bash
#@lisbeth ASIX-M01
#Febrer 2024
#Descripción:Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar.El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
#
#-------------------------------
if [ $# -ne 1 ];then
   echo "Error número de argument."
   echo "Usage: $0 argument."
   exit 1
fi
   
MAX=$1
num=1
while [ $num -le $MAX ]
do 
    read -r line
    echo $num: $line
     ((num++))
done 
exit 0

