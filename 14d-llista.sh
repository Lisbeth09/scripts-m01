#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:
#a)rep un arg i és un directori i es llista
#b) llistar numerant els elements del dir.
#c) per cada element dir si es dir,regular, o altra cosa.
#d) es reben n directoris.
#-------------------
#validar que hi ha arg
ERR_NARGS=1
ERR_NODIR=2
if [ $# -eq 0 ]; then
  echo "Error número de arguments"
  echo "Usage: $0 dir"
  exit $ERR_NARGS 
fi

#iterar per cada argument
dir=$1
for dir in $* 
do 
 if [ ! -d $dir ];then
   echo "Error: $dir Nodirectori" >> /dev/stderr
 else 
 # xisxa:llistar 
   llista_dir=$(ls $dir)
   echo "Llistat: $dir-------"
   for elem in $llista_dir
   do 
     if [ -f  $dir/$elem ]; then
        echo "$echo is regular file"
     elif [ -d $di/$elem ];then
       echo "$elem is a directory"
     else
        echo "$elem is another thing." 
     fi
    done
fi
done 
exit 0
