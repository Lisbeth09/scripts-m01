#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:
#
#-------------------
# 
#1)Validar args
if [ $# -eq 0 ]; then
        echo "Error num args"
        echo "Usage: $0 mesos.."
        exit 1
fi
#2) Iterar la llista d'arg
for mesos in $*
do
 if ! [ $mesos -gt 0 -a $mesos -le 12 ]; then
        echo "Error:El número  $mesos no valit , introdueix un número del rang (0-12)" >> /dev/stderr
 else
  case $mesos in 
     "2") 
        dies=28;;
     "4"|"6"|"9"|"11")
        dies=30;;
     *)
        dies=31;;
   esac
   echo " El mes $mesos té $dies dies."
   fi	
done 
exit 0

