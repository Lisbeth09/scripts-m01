#! /bin/bash
#@lisbeth ASIX-M01
#Febrer 2024
#Descripción: Procesa línia a línia l'entrada estandar,si la línia té més de 60 caràcters la mostra ,si no no.
#
#-------------------------------
while read -r line 
do 
char=$(echo $line | wc -c)
   if [ $char -gt 60 ]
    then
     echo $line
   fi 
done
exit 0


