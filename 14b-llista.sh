#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:
#rep un arg i és un directori i es llista
#-------------------
#validar que hi ha arg
if [ $# -ne 1 ]; then
  echo "Error número de arguments"
  echo "Usage: $0 dir"
  exit $ERR_NARGS 
fi

#Validar que es un dir

if [ ! -d $dir ];then
 echo "Error:$dir no és un directori."
 echo "usage.$0 dir"
 exit $ERR_NODIR
fi 
# xisxa:llistar 
llista=$(ls $dir)
num=1
for elem in $llista
do 
    echo $num: $elem 
     ((num++))
done 
exit 0
