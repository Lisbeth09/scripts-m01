#! /bin/bash
#@lisbeth ASIX-M01
#Febrer 2024
#Descripción: Fer un programa que rep per stdin noms d'usuari (un per línia), si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr
#
#-------------------------------
while read -r user 
do  
    grep -q "^$user:" /etc/passwd
    if [ $? -ne 0 ]
    then 
      echo "Error: El $user no existeix en el sistema.">> /dev/stderr
    else 
      echo $user
    fi
done
exit 0
 

