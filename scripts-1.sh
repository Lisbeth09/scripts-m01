#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:
#
#-------------------
#8.un fitxer que procesa el contingut de una matricula AAAA999.
#ok stdout,err stderr
#retorna 0,3 error
estatus=0
fileIn=$1
while read -r line
do
        echo $line | grep -E "^[A-Z,a-z]{4}[0-9]{3}$"
        if [ $? -ne 0 ]
        then
          echo "$line" >> /dev/stderr
          estatus=3
        fi
done < $fileIn          
exit $estatus
#7 
estatus=0
if [ $# -ne 5 ]
then
   estatus=1
   exit $estatus 
fi
flag=$1
shift
for arg in $*
do    
      test $flag $arg
        if [ $? -ne 0 ]
        then
          estatus=2 
	fi
done
exit $estatus 


#6) Processar per stdin línues d'entrada tipus "Tom Snyder" i mostrar per stdout la línia en format => T.Snyder.
while read -r line 
do
      echo $line | grep "^[A-Z]."
      if [ $? -eq 0 ]
      then
      echo $line | tr ' ' ':' | sed -r 's/^([A-Z])[^:]*:([^:]*)$/\1.\2/'
      fi
done
exit 0
#5)Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
while read -r line
do
	echo $line | grep -E "^.{,50}$"
done
exit 0
#4)Procesar stdin mostrant per stdout les línies numerades i en majúscules.
num=1
while read -r line
do
       echo $num: $line | tr 'a-z' 'A-Z'
        ((num++))
done	
exit 0 	

#3) Processar arguments que són matricules:
#a)Llistar les vàlides,del tipus: 9999-AAA.
#b)stdout les que són vàlides,per stderr les no vàlides.Retorna de status el número d'errors (de no vàlides).
if [ $# -eq 0 ];then
	echo "Error núm arguments."
	echo "Usage: $0 arg."
	exit 1
fi
err=0
for matricula in $*
do 
      echo $matricula | grep -E "^[0-9]{4}-[A-Z]{3}$"
      if [ $? -ne 0 ] 
      then 
	    echo "Error: $matricula" >>/dev/stderr
	       ((err++))
      fi
done
exit $err



#2) Processar els arguments i comptar quantes n'hi ha de 3 o més caràcters.
if [ $# -eq 0 ];then
	echo "Error núm arguments."
	echo "Usage: $0 arg."
	exit 1
fi
num=0
for arg in $*
do 
      echo "$arg" | grep -E -q ".{3,}"
      if [ $? -eq 0 ]
      then
	  ((num++))
      fi
done
echo $num 
exit 0

#1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
if [ $# -eq 0 ];then
     echo "Error núm arguments."
     echo "Usage: $0 arg."
     exit 1
fi
for arg in $* 
do
     echo "$arg" | grep -E ".{4,}"
     if [ $? -eq 0 ];then
     fi
done 
exit 0
