#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:4. Fer un programa que rep com a arguments números de més (un o més) i 
#indica per a cada mes rebut quants dies té el més.
#
#-------------------
if [ $# -eq 0 ];then 
    echo "Error número de arguments"
    echo "Usage: $0 mes."
    exit 1
fi

for mesos in $*
do 
   if ! [ $mesos -gt 0 -a $mesos -le 12 ]; then
      echo "Error: El número $mesos no valit, introdueix un número entre el rang (1-12)." >> /dev/stderr
   
   else 
     case $mesos in 
      "2")
        dies=28;;
      "4"|"6"|"9"|"11")
        dies=30;;
       *)
         dies=31;;
     esac 
     echo "El $mesos té $dies dies."
    fi
done
exit 0 


