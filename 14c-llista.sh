#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:
#a)rep un arg i és un directori i es llista
#b) llistar numerant els elements del dir.
#c) per cada element dir si es dir,regular, o altra cosa.
#-------------------
#validar que hi ha arg
if [ $# -ne 1 ]; then
  echo "Error número de arguments"
  echo "Usage: $0 dir"
  exit $ERR_NARGS 
fi

#Validar que es un dir

if [ ! -d $dir ];then
 echo "Error:$dir no és un directori."
 echo "usage.$0 dir"
 exit $ERR_NODIR
fi 
# xisxa:llistar 
llista_dir=$(ls $dir)
for elem in $llista_dir
do 
   
    if [ -f  $dir/$elem ]; then
      echo "$elem is regular file"
    elif [ -d $di/$elem ];then
      echo "$elem is a directory"
    else
       echo "$elem is another thing." 
done 
exit 0
