#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció: Mostrar línia a línia l'entrada estàndar,retallant només els primers 50 caràcter.
#
#-------------------

linia=$1
while read -r line 
do
    echo "$line" | cut -c 1-50  
done 
exit 0
