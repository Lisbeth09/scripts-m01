# /bin/bash 
# Lisbeth Chasi 
# 
function disk_repport(){
	if [ $# -lt 2 ]
	then	
		echo "Usage:$1"
		echo "Error: num arg."
		return 1
	fi
	ERR_DIR=2
	
	if [ ! -d $1 ]
	then
		echo "Error el dir no existeix."
		return $ERR_DIR
	fi 
	cont_err=0
	dir_desti=$1
	shift
	for elem in $* 
	do
		if [ ! -e $elem ]
		then
			echo "Error $elem no existe.">>/dev/stderr
			
		else 
			file -b $elem | grep -q "^ELEM"
			if [ $? -eq 0 ]
			then 
				cp $elem $dir_desti
				echo "$elem"	
			else	
				((cont_err++))
				status=3
			fi


		fi

	done
	echo $cont_err
	$status

}
