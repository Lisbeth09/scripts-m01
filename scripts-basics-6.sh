#! /bin/bash
#@lisbeth ASIX-M01
#Febrer 2024
#Descripción: Fer un programa que rep com a arguments noms de dies de la setmana i mostra quants dies eren laborables i quants festius.Si l'argument no és un dia de la setmana genera un error per stderr.
#
#-------------------------------
  

diaslaborables=0
findesemana=0
for dias in $*
do
  case $dias in
  "lunes"|"martes"|"miercoles"|"jueves"|"viernes")
          ((diaslaborables ++))  ;;
          "sabado"|"domingo")
            ((findesemana++))   ;;
             *)
               echo "Esto ($dias) no és un dia." >> /dev/stderr;;
  esac
done
echo "dias laborables: $diaslaborables"
echo "dias de fin de semana: $findesemana"
exit 0


