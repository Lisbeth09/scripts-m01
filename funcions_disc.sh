#! /bin/bash
#funcions de disc
#@Lisbeth Chasi April 2024
#-------
#Procesar el fitxer rebut o stdin si no en rep cap.
function loginboth(){
	fileIn="/dev/stdin"
	if [ $# -eq 1 ]
	then 
		fileIn=$1
	fi
	while read -r login
	do
		fsize $login
	done < $fileIn
}
#rep logins per la entrada standar, i fsize per cada un.
function loginstdin(){
	while read -r login
	do 
		fsize $login
	done	
}
#Rep arg fitxer,cada linia del fitxer té un login.
function loginfile(){
	if [ ! -f $1 ]
	then   
		echo "Error"
		return 1 
	fi
	fileIn=$1
	while read -r login
	do 
		fsize $login
	done < $fileIn	
}
#Utilitza la altra funcio.
function loginargs(){
	if [ $# -eq 0 ]
	then
		echo "Error num arg."
		echo "Usage:"
		return 1
	fi
	for login in $* 
	do 
		fsize $login
	done
}
#rep login i calcula el fsize de home 
function fsize(){
	login=$1
	line=$(grep "^$login:" /etc/passwd)
	if [ -z "$line" ]
	then 
		echo "Error"
		return 1 
	fi
	dirHome=$(echo $line | cut -d: -f6)
	du -sh $dirHome
}
