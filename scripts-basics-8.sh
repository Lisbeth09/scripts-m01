#! /bin/bash
#@lisbeth ASIX-M01
#Febrer 2024
#Descripción:Fer un programa que rep com a argument noms d'usuari,si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout.Si no existeix el mostra per stderr.
#
#-------------------------------
if [ $# -eq 0 ]; then 
  echo "Error número de arguments."
  echo "Usage:$0 argument."
  exit 1
fi 

for user in $*
do 
    grep -q "^$user:" /etc/passwd
    if  [ $? -ne 0 ]
      then
        echo "Error: El $user no existeix en el sistema." >> /dev/stderr
    else 
       echo $user
    fi
done 
exit 0
