#! /bin/bash
#@Lisbeth ASIX-M01
#Febrer 2024
#Descripció:for-notes..
#contesta si esta sùspes,aprovat,excel-lent
#-------------------
#1)Validar args
if [ $# -eq 0 ]; then 
	echo "Error num args"
	echo "Usage: $0 nota.."
	exit 1
fi
#2) Iterar la llista d'arg
for nota in $*
do
 if ! [ $nota -ge 0 -a $nota -le 10 ]; then
	echo "Error: nota $nota no valida(0-10)" >> /dev/stderr 
 else
   if [ $nota -lt 5 ]; then
     echo "suspès"
   elif [ $nota -lt 7 ];then 
     echo "aprovat"
   else
     echo "notable"
   fi
 fi	
done 
exit 0
